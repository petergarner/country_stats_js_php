
<?php
    $url_uk = 'uk.json';
    $string_uk = file_get_contents($url_uk);
    $uk_json = json_decode($string_uk, true);

    $url_fr = 'fr.json';
    $string_fr = file_get_contents($url_fr);
    $fr_json = json_decode($string_fr, true);

    $url_it = 'it.json';
    $string_it = file_get_contents($url_it);
    $it_json = json_decode($string_it, true);

    $url_gm = 'gm.json';
    $string_gm = file_get_contents($url_gm);
    $gm_json = json_decode($string_gm, true);

    // set via country checkbox selections
    $uk_th = isset($_GET['check_uk']) ? "UK" : "";
    $france_th = isset($_GET['check_fr']) ? "France" : "";
    $italy_th = isset($_GET['check_it']) ? "Italy" : "";
    $germany_th = isset($_GET['check_gm']) ? "Germany" : "";

    $countries = array($uk_th,$france_th,$italy_th,$germany_th);
    
    foreach ($countries as $key) {  // part of country selection check,
        $countSel += strlen($key); // really just looking for > 0
    }
    
    // set via form submission
    $check_form = isset($_GET['formSub']) ? 1 : 0;
    
    // set via statistics checkbox selections
    $check_BudgSurp = isset($_GET['surplus']) ? 1 : 0;
    $check_pg = isset($_GET['Popgrow']) ? 1 : 0;
    $check_net_pop = isset($_GET['net_pop']) ? 1 : 0;
    $check_pop_tot = isset($_GET['pop_tot']) ? 1 : 0;
    $check_Labor_agri = isset($_GET['Labor_agri']) ? 1 : 0;
    $check_Labor_ind = isset($_GET['Labor_ind']) ? 1 : 0;
    $check_Labor_serv = isset($_GET['Labor_serv']) ? 1 : 0;
    $check_unemp_rate = isset($_GET['unemp_rate']) ? 1 : 0;
    $check_revenue = isset($_GET['revenue']) ? 1 : 0;
    $check_budge_exp = isset($_GET['expend']) ? 1 : 0;

    $enter_form = "yes"; // allows header entry after alert

    $stats = array($check_BudgSurp, $check_pg, $check_net_pop, $check_pop_tot, $check_Labor_agri, $check_Labor_ind, $check_Labor_serv, $check_unemp_rate, $check_revenue, $check_budge_exp);

    $sum_stats = array_sum($stats); // value used for two checks below

    if ($sum_stats > 0) {   // prevents creation of empty cell if no statistics chosen
        $empty_header = 1;
    }
    
    if ($sum_stats > 5) {
        $alert5stats = 1; // toggles to 1 or "" depending on whether too 
        $check_BudgSurp = 0;  // many statistics checkboxes selected
        $check_pg = 0;
        $check_net_pop = 0;
        $check_pop_tot = 0;
        $check_Labor_agri = 0;   //
        $check_Labor_ind = 0;    // ensures all values set to empty after
        $check_Labor_serv = 0;   // alert, preventing repeat alerts
        $check_unemp_rate = 0;   //
        $check_revenue = 0;
        $check_budge_exp = 0;
        $uk_th = "";
        $france_th = "";
        $italy_th = "";
        $germany_th = "";   
    } else {
        $alert5stats = "";
    }

    $jsons = array($uk_json,$fr_json,$it_json,$gm_json);

     if ($check_BudgSurp == 1)  { // if the statistic was selected...
        $a = array();
        foreach ($jsons as $jkey) { 
            array_push($a,$jkey["Economy"]["Budget surplus (+) or deficit (-)"]["text"]);  //  collects text field values from json files
        }
        list($ukBudgSurp,$frBudgSurp,$itBudgSurp,$gmBudgSurp) = $a; // converts array values (text from json files) to variables
    }
     if ($check_pg == 1)  {
        $a = array();
        foreach ($jsons as $jkey) {
            array_push($a,$jkey["People and Society"]["Population growth rate"]["text"]);
        }
        list($ukPopgrow,$frPopgrow,$itPopgrow,$gmPopgrow) = $a;
    }

     if ($check_net_pop == 1)  {
        $a = array();
        foreach ($jsons as $jkey) {
            array_push($a,$jkey["Communications"]["Internet users"]["total"]["text"]);
        }
        list($uknet_pop,$frnet_pop,$itnet_pop,$gmnet_pop) = $a;
    }

     if ($check_pop_tot == 1)  {
        $a = array();
        foreach ($jsons as $jkey) {
            array_push($a,$jkey["People and Society"]["Population"]["text"]);
        }
        list($ukpop_tot,$frpop_tot,$itpop_tot,$gmpop_tot) = $a;
    }

     if ($check_Labor_agri == 1)  {
        $a = array();
        foreach ($jsons as $jkey) {
            array_push($a,$jkey["Economy"]["Labor force - by occupation"]["agriculture"]["text"]);
        }
        list($ukLabor_agri,$frLabor_agri,$itLabor_agri,$gmLabor_agri) = $a;
    }

     if ($check_Labor_ind == 1)  {
        $a = array();
        foreach ($jsons as $jkey) {
            array_push($a,$jkey["Economy"]["Labor force - by occupation"]["industry"]["text"]);
        }
        list($ukLabor_ind,$frLabor_ind,$itLabor_ind,$gmLabor_ind) = $a;
    }

     if ($check_Labor_serv == 1)  {
        $a = array();
        foreach ($jsons as $jkey) {
            array_push($a,$jkey["Economy"]["Labor force - by occupation"]["services"]["text"]);
        }
        list($ukLabor_serv,$frLabor_serv,$itLabor_serv,$gmLabor_serv) = $a;
    }

     if ($check_unemp_rate == 1)  {
        $a = array();
        foreach ($jsons as $jkey) {
            array_push($a,$jkey["Economy"]["Unemployment rate"]["text"]);
        }
        list($ukunemp_rate,$frunemp_rate,$itunemp_rate,$gmunemp_rate) = $a;
    }

     if ($check_revenue == 1)  {
        $a = array();
        foreach ($jsons as $jkey) {
            array_push($a,$jkey["Economy"]["Budget"]["revenues"]["text"]);
        }
        list($ukrevenue,$frrevenue,$itrevenue,$gmrevenue) = $a;
    }

     if ($check_budge_exp == 1)  {
        $a = array();
        foreach ($jsons as $jkey) {
            array_push($a,$jkey["Economy"]["Budget"]["expenditures"]["text"]);
        }
        list($ukexpend,$frexpend,$itexpend,$gmexpend) = $a;
    }
?>
