<?php include "my_php.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>country data</title>
<style>

    #myTable {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #myTable td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }
    #text { font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;}
    #myTable tr {background-color: #f2f2f2;}

    #myTable th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }

    #mainbody { border: 20px solid #FFFFFF; }
    #vals {
        background-color: #4CAF50;
        border: none;
        color: white;
        padding: 8px 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 8px 2px;
    } 
</style>
</head>
    
<body id="mainbody"> 
    <h2 id="text">CIA Country Facts</h2>
    <form id="mainForm" action="<?php echo $_SERVER['PHP_SELF'] ?>" name="theform" method="GET">
        <!-- The above 'aftion' reflects the my_php.php files back into this php file at exactly this point -->
        <label id="text">United Kingdom<input type="checkbox" name="check_uk" ></label>
        <label id="text">France<input type="checkbox" name="check_fr" value="on"></label>
        <label id="text">Italy<input type="checkbox" name="check_it"></label>
        <label id="text">Germany<input type="checkbox" name="check_gm"></label>
        <br/>
        <br/>
        <label id="text">Budget Surplus<input type="checkbox" name="surplus"></label>
        <br/>
        <label id="text">Population Growth Rate<input type="checkbox" name="Popgrow"></label>
        <br/>
        <label id="text">Internet Users<input type="checkbox" name="net_pop"> </label>
        <br/>
        <label id="text">Total Population<input type="checkbox" name="pop_tot"></label>
        <br/>
        <label id="text">Labor Force - Ariculture<input type="checkbox" name="Labor_agri"></label>
        <br/>
        <label id="text">Labor Force - Industry<input type="checkbox" name="Labor_ind"></label>
        <br/>
        <label id="text">Labor Force - Services<input type="checkbox" name="Labor_serv"></label>
        <br/>
        <label id="text">Unemployment Rate<input type="checkbox" name="unemp_rate"></label>
        <br/>
        <label id="text">Budget Revenues 2016<input type="checkbox" name="revenue"></label>
        <br/>
        <label id="text">Budget Expenditures 2016 (est)<input type="checkbox" name="expend"></label>
        <br/>
        <br/>
        <input type="submit" value="Submit" name="formSub" id="vals">
        <button onclick="myFunction()" id="vals" >Clear All</button> 
    </form>


    <script>
        function myFunction() {
            document.getElementById("mainForm").reset();
            var tab = document.getElementById("lowdiv"); {
            tab.style.display = "none"; 
            }   // clears (hides) the form until new data
        }
    </script>
    
    <?php if ($check_form === 1) {  // checks the form has in fact been sumbitted 
        if ($alert5stats === 1) { // see my_php.php for deatils on boolean check variables
             echo '<script> alert("Choose up to 5 Statistics of less") </script>';
            $enter_form = "no";
         }
         if ($countSel < 1) {
             echo '<script> alert("Choose at least 1 Country") </script>';
             $enter_form = "no";
         }
        if ($sum_stats < 1) {
             echo '<script> alert("Choose at least 1 Statistic") </script>';
            $enter_form = "no";
         }
    }?>
    <br/>
    <br/>   
        <div id="lowdiv">
        <table id="myTable" >
          <tr>
              <?php  // see my_php.php for deatils on boolean check variables
                if ($empty_header === 1 and $countSel != 0 and $enter_form === "yes") { 
                echo "<th>  </th>"; 
                if ($uk_th != "") { echo "<th>$uk_th</th>"; } 
                if ($france_th != "") { echo "<th>$france_th</th>"; }
                if ($italy_th != "") { echo "<th>$italy_th</th>"; }
                if ($germany_th != "") { echo "<th>$germany_th</th>"; }
              }
              ?>
          </tr>
          <tr>
              <?php // all $check_<name> varaibles are values set is form $_GET() process
              if ($check_BudgSurp === 1 and $countSel != 0) { 
                echo "<td>Budget Surpus</td>"; 
                if ($uk_th != "") { echo "<td>$ukBudgSurp</td>"; }
                if ($france_th != "") { echo "<td>$frBudgSurp</td>"; }
                if ($italy_th != "") { echo "<td>$itBudgSurp</td>"; }
                if ($germany_th != "") { echo "<td>$gmBudgSurp</td>"; }
                }
              ?>
          </tr>
          <tr>
              <?php
              if ($check_pg === 1 and $countSel != 0) { 
                  echo "<td>Population Growth</td>"; 
                  if ($uk_th != "") { echo "<td>$ukPopgrow</td>"; }
                  if ($france_th != "") { echo "<td>$frPopgrow</td>"; }
                  if ($italy_th != "") { echo "<td>$itPopgrow</td>"; }
                  if ($germany_th != "") { echo "<td>$gmPopgrow</td>"; }
              }
              ?>
          </tr>
          <tr>
              <?php
              if ($check_net_pop === 1 and $countSel != 0) { 
                  echo "<td>Internat Users</td>"; 
              if ($uk_th != "") { echo "<td>$uknet_pop</td>"; }
              if ($france_th != "") { echo "<td>$frnet_pop</td>"; }
              if ($italy_th != "") { echo "<td>$itnet_pop</td>"; }
              if ($germany_th != "") { echo "<td>$gmnet_pop</td>"; }
                  }
              ?>
          </tr>
          <tr>
              <?php
              if ($check_pop_tot === 1 and $countSel != 0) { 
                  echo "<td>Total Population</td>"; 
              if ($uk_th != "") { echo "<td>$ukpop_tot</td>"; }
              if ($france_th != "") { echo "<td>$frpop_tot</td>"; }
              if ($italy_th != "") { echo "<td>$itpop_tot</td>"; }
              if ($germany_th != "") { echo "<td>$gmpop_tot</td>"; }
                  }
              ?>
          </tr>
          <tr>
              <?php
              if ($check_Labor_agri === 1 and $countSel != 0) { 
                  echo "<td>Agricauture Labor Force</td>"; 
              if ($uk_th != "") { echo "<td>$ukLabor_agri</td>"; }
              if ($france_th != "") { echo "<td>$frLabor_agri</td>"; }
              if ($italy_th != "") { echo "<td>$itLabor_agri</td>"; }
              if ($germany_th != "") { echo "<td>$gmLabor_agri</td>"; }
                  }
              ?>
          </tr>
          <tr>
              <?php
              if ($check_Labor_ind === 1 and $countSel != 0) { 
                  echo "<td>Industry Labor Force</td>"; 
              if ($uk_th != "") { echo "<td>$ukLabor_ind</td>"; }
              if ($france_th != "") { echo "<td>$frLabor_ind</td>"; }
              if ($italy_th != "") { echo "<td>$itLabor_ind</td>"; }
              if ($germany_th != "") { echo "<td>$gmLabor_ind</td>"; }
                  }
              ?>
          </tr>
          <tr>
              <?php
              if ($check_Labor_serv === 1 and $countSel != 0) { 
                  echo "<td>Services Labor Force</td>"; 
              if ($uk_th != "") { echo "<td>$ukLabor_serv</td>"; }
              if ($france_th != "") { echo "<td>$frLabor_serv</td>"; }
              if ($italy_th != "") { echo "<td>$itLabor_serv</td>"; }
              if ($germany_th != "") { echo "<td>$gmLabor_serv</td>"; }
                  }
              ?>
          </tr>
          <tr>
              <?php
              if ($check_unemp_rate === 1 and $countSel != 0) { 
                  echo "<td>Unemployment Rate </td>"; 
              if ($uk_th != "") { echo "<td>$ukunemp_rate</td>"; }
              if ($france_th != "") { echo "<td>$frunemp_rate</td>"; }
              if ($italy_th != "") { echo "<td>$itunemp_rate</td>"; }
              if ($germany_th != "") { echo "<td>$gmunemp_rate</td>"; }
              }
              ?>
          </tr>
            <tr>
              <?php
              if ($check_revenue === 1 and $countSel != 0) { 
                  echo "<td>Tax Revenues</td>"; 
              if ($uk_th != "") { echo "<td>$ukrevenue</td>"; }
              if ($france_th != "") { echo "<td>$frrevenue</td>"; }
              if ($italy_th != "") { echo "<td>$itrevenue</td>"; }
              if ($germany_th != "") { echo "<td>$gmrevenue</td>"; }
              }
              ?>
          </tr>
          <tr>
              <?php
              if ($check_budge_exp === 1 and $countSel != 0) { 
                  echo "<td>Budget Expenditures</td>"; 
              if ($uk_th != "") { echo "<td>$ukexpend</td>"; }
              if ($france_th != "") { echo "<td>$frexpend</td>"; }
              if ($italy_th != "") { echo "<td>$itexpend</td>"; }
              if ($germany_th != "") { echo "<td>$gmexpend</td>"; }
              }
              ?>
          </tr>      
        </table>
        </div>
    </body>
</html>
